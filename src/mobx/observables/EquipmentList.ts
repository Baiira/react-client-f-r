import {action, observable} from 'mobx';
import Equipment from './Equipment';
import EquipmentService from '../../http/EquipmentService';

class EquipmentList {
	@observable equipments: Equipment[];

	constructor() {
		this.equipments = [];
	}

	/**
	 * Reset the equipments in order to prevent cache bugs.
	 */
	@action resetEquipments = () => {this.equipments = [];};

	/**
	 * Delegate the information retrieval to the EquipmentService and add them to the equipments list.
	 * @param userId
	 */
	@action getAllEquipments = (userId) => {
		EquipmentService.getAllEquipments(userId).then(res => {
			if (EquipmentList.isResValid(res)) {
				res.forEach(val => {
					this.updateOrCreateEquipment(val);
				})
			}
		});
	};

	/**
	 * Delegate the information update to the EquipmentService and update the equipments list.
	 * @param equipmentId
	 * @param userId
	 */
	@action returnOne = (equipmentId, userId) => {
		EquipmentService.returnOne(equipmentId, userId).then(res => {
			if (EquipmentList.isResValid(res)) {
				res.taken = false;
				this.updateOrCreateEquipment(res);
			}
		})
	};

	/**
	 * Delegate the information update to the EquipmentService and update the equipments list.
	 * @param equipmentId
	 * @param userId
	 */
	@action takeOne = (equipmentId, userId) => {
		EquipmentService.takeOne(equipmentId, userId).then(res => {
			if (EquipmentList.isResValid(res)) {
				res.taken = true;
				this.updateOrCreateEquipment(res);
			}
		})
	};

	private findById(id) {
		return this.equipments.findIndex((value) => id == value.id);
	}

	private updateOrCreateEquipment(equipment) {
		const index = this.findById(equipment.id);
		if (index > -1) {
			this.equipments[index] = equipment;
		} else {
			this.equipments.push(new Equipment(equipment.id, equipment.name, equipment.left, equipment.taken));
		}
	}

	private static isResValid(res) {
		return res != null || res != undefined;
	}
}

const equipmentList = new EquipmentList();

export default equipmentList;