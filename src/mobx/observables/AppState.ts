import {action, observable} from 'mobx';
import AuthenticationService from '../../http/AuthenticationService';
import userState from './UserState';
import equipmentList from './EquipmentList';

class AppState {
	@observable connected;

	constructor() {
		this.connected = false;
	}

	/**
	 * Call the AuthenticationService in order to log the user.
	 * @param image
	 */
	@action tryAndConnect = (image) => {
		AuthenticationService.logMe(image).then(res => {
			if (res && res.id) {
				userState.connectUser(res.id);
				this.connected = true;
			}
		});
	};

	/**
	 * Basically disconnect the user, delegating resets to userState and equipmentList.
	 */
	@action disconnect = () => {
		userState.disconnectUser();
		equipmentList.resetEquipments();
		this.connected = false;
	};
}

const appState = new AppState();

export default appState;