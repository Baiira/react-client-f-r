import {action, observable} from 'mobx';
import {default as env} from '../../assets/environment';

class ErrorState {
	@observable errorMessage = "";
	@observable isVisible = false;
	constructor() {}

	/**
	 * Set the given errorMessage and passes the isVisible flag to true.
	 * @param errorMessage
	 */
	@action printErrorWithMessage = (errorMessage) => {
		this.errorMessage = errorMessage;
		this.isVisible = true;
	};

	/**
	 * Set a default errorMessage and passes the isVisible flag to true.
	 */
	@action printErrorWithDefaultMessage = () => {
		this.errorMessage = env.trads.DEFAULT_ERROR_MESSAGE;
		this.isVisible = true;
	};

	/**
	 * Basically dismiss the error resetting the errorMessage and passes the isVisible to false.
	 */
	@action dismissError = () => {
		this.errorMessage = "";
		this.isVisible = false;
	}
}

const errorState = new ErrorState();

export default errorState;