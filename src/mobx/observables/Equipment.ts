import {observable} from 'mobx';

export default class Equipment {
	@observable id;
	@observable name;
	@observable left;
	@observable taken;

	constructor(id, name, left, taken) {
		this.id = id;
		this.name = name;
		this.left = left;
		this.taken = taken;
	}
}