import {action, observable} from 'mobx';
class UserState {
	@observable userId;

	constructor() {
		this.userId = null;
	}

	@action connectUser = (id) => {this.userId = id;};

	@action disconnectUser = () => {this.userId = null;};
}

const userState = new UserState();

export default userState;