import './App.css';
import Home from './components/home/Home';
import ViewSelect from './components/view-select/ViewSelect';
import * as React from "react";
import {observer} from 'mobx-react';
import {ErrorComponent} from './components/ErrorComponent';

@observer
class App extends React.Component<any> {

	render() {
		const view = !this.props.appState.connected ?
			<Home appState={this.props.appState}/> :
			<ViewSelect appState={this.props.appState}/>;

		const error = this.props.errorState.isVisible ?
			<ErrorComponent errorState={this.props.errorState}/> :
			null;

		return (
			<div className="App">
				{view}
				{error}
			</div>
		);
	}
}

export default App;
