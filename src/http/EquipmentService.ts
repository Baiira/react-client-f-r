import {default as env} from '../assets/environment'
import {HttpHelper} from './HttpHelper';

export default class EquipmentService {
	private static readonly EQUIPMENT_URL = env.servers.rest.url + env.servers.rest.paths.equipments.default;
	private static readonly TAKE_ONE_METHOD = env.servers.rest.paths.equipments.takeOne.method;
	private static readonly TAKE_ONE_URL = EquipmentService.EQUIPMENT_URL + env.servers.rest.paths.equipments.takeOne.url;
	private static readonly GET_ALL_METHOD = env.servers.rest.paths.equipments.getAll.method;
	private static readonly GET_ALL_URL = EquipmentService.EQUIPMENT_URL + env.servers.rest.paths.equipments.getAll.url;
	private static readonly RETURN_ONE_URL = EquipmentService.EQUIPMENT_URL + env.servers.rest.paths.equipments.returnOne.url;
	private static readonly RETURN_ONE_METHOD = env.servers.rest.paths.equipments.returnOne.method;
	private static readonly ID = '{id}';
	private static readonly USER_ID = '{userId}';

	/**
	 * Will send a HTTP GET request to the server equipment endpoint.
	 * It will return the list for all the equipments and if the user have already took them.
	 *
	 * @param userId
	 * @return {Promise<any>}
	 */
	public static async getAllEquipments(userId) {
		const res = await fetch(
			EquipmentService.GET_ALL_URL.replace(EquipmentService.USER_ID, userId),
			HttpHelper.buildRequest(EquipmentService.GET_ALL_METHOD));

		return await res.json();
	}

	/**
	 * Will send a HTTP PUT request to the server equipment endpoint,
	 * that will decrement the left available for the current Equipment.
	 * It will return the equipment with it's new values.
	 *
	 * @param equipmentId
	 * @param userId
	 * @return {Promise<any>}
	 */
	public static async takeOne(equipmentId, userId) {
		const res = await fetch(
			EquipmentService.TAKE_ONE_URL.replace(EquipmentService.ID, equipmentId.toString()).replace(EquipmentService.USER_ID, userId),
			HttpHelper.buildRequest(EquipmentService.TAKE_ONE_METHOD));

		return await res.json();
	}

	/**
	 * Will send a HTTP DELETE request to the server equipment endpoint,
	 * that will increment the left available for the current Equipment.
	 * It will return the equipment with it's new values.
	 *
	 * @param equipmentId
	 * @param userId
	 * @return {Promise<any>}
	 */
	public static async returnOne(equipmentId, userId) {
		const res = await fetch(
			EquipmentService.RETURN_ONE_URL.replace(EquipmentService.ID, equipmentId.toString()).replace(EquipmentService.USER_ID, userId),
			HttpHelper.buildRequest(EquipmentService.RETURN_ONE_METHOD));

		return await res.json();
	}
}