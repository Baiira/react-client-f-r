
export class HttpHelper {
	private static readonly DEFAULT_CORS = {
		mode: 'cors',
		headers: {
			'Access-Control-Allow-Origin': '*',
			'Accept': 'application/json, text/plain, */*',
			'Content-Type': 'application/json'
		},
		cache: 'default'
	};

	/**
	 * Builder for CORS valid request.
	 *
	 * @param method The request method should be one of: PUT, POST, GET, DELETE, OPTIONS.
	 * @return The right formatted request.
	 */
	public static buildRequest(method) {
		return Object.assign({
			method: method
		}, HttpHelper.DEFAULT_CORS)
	}
}