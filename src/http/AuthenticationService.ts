import {default as env} from '../assets/environment'
import {HttpHelper} from './HttpHelper';
import errorState from '../mobx/observables/ErrorState';

export default class AuthenticationService {
	private static readonly AUTH_URL = env.servers.rest.url + env.servers.rest.paths.authentication.default;
	private static readonly LOGIN_URL = AuthenticationService.AUTH_URL + env.servers.rest.paths.authentication.login.url;
	private static readonly LOGIN_METHOD = env.servers.rest.paths.authentication.login.method;

	/**
	 * Will send a HTTP POST to the server authentication endpoint.
	 * If the server respond with an error it will be delegate to the errorState.
	 *
	 * @param image
	 * @return {Promise<any>}
	 */
	public static async logMe(image) {
		const request = Object.assign({}, HttpHelper.buildRequest(AuthenticationService.LOGIN_METHOD),
			{
				body: JSON.stringify({image: image})
			});

		const response = await fetch(AuthenticationService.LOGIN_URL, request);

		const json = await response.json();
		if (json && json.message) {
			errorState.printErrorWithMessage(json.message);
		}

		return json;
	}
}