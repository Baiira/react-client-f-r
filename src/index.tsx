import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import appState from './mobx/observables/AppState';
import errorState from './mobx/observables/ErrorState';

ReactDOM.render(
  <App appState={appState} errorState={errorState}/>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
