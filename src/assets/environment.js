export default {
  servers: {
    rest: {
      url: "http://localhost:4000",
      paths: {
        authentication: {
          default: "/auth",
          login: {
            method: "POST",
            url: "/"
          }
        },
        equipments: {
          default: "/equipment",
          getAll: {
            method: "GET",
            url: "/byuser/{userId}"
          },
          takeOne: {
            method: "PUT",
            url: "/take/{id}/{userId}"
          },
          returnOne: {
            method: "DELETE",
            url: "/return/{id}/{userId}"
          }
        }
      }
    }
  },
  trads: {
    NO_MATCHING_USER_FOUND_EXCEPTION: "Aucun utilisateur n\'a été trouver, veuillez recommencer.",
    DEFAULT_ERROR_MESSAGE: "Woups, quelque chose de bizarre vient de se produire !"
  }
}