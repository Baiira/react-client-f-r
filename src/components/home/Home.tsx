import * as React from 'react';
import Webcam from '../webcam/ReactWebcam';

class Home extends React.Component<any> {
	webcam = null;

	constructor(props) {
		super(props);
		this.state = {
			screenshot: null
		};
	}

	/**
	 * Click handler for the connexion button, should take a screen shot.
	 * From the screenshot will delegate the connexion to appState.
	 */
  handleClick = () => {
		const screenshot = this.webcam.getScreenshot();
		this.props.appState.tryAndConnect(screenshot);
	};

  render() {
    const btnStyle = {
      position: "absolute" as "absolute",
      right: '150px',
      top: '50px',
    };

    return (
      <div>
        <Webcam ref={node => this.webcam = node}/>
        <button onClick={this.handleClick.bind(this)} style={btnStyle}>S'identifier</button>
      </div>
    );
  }
}

export default Home;
