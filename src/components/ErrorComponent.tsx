import * as React from "react";
import {default as env} from '../assets/environment'

export class ErrorComponent extends React.Component<any> {

	render() {
		const errorMessage = env.trads[this.props.errorState.errorMessage] !== undefined ?
			env.trads[this.props.errorState.errorMessage] :
			env.trads.DEFAULT_ERROR_MESSAGE;
		const modalStyle = {
			zIndex: 1000,
			backgroundColor: "#3d3d3d",
			opacity: 0.85,
			position: 'absolute' as 'absolute',
			height: '100%',
			width: '100%'
		};
		return (
			<div style={modalStyle} onClick={this.props.errorState.dismissError}>
				<div className="modal-dialog modal-dialog-centered">
					<div className="alert alert-danger">
						<h4>{errorMessage}</h4>
						<p>Cliquez n'importe où pour faire disparaitre ce message.</p>
					</div>
				</div>
			</div>
		);
	}
}