import * as React from 'react';
import {observer} from 'mobx-react';
import equipmentList from '../../../mobx/observables/EquipmentList';

@observer
class EquipmentDetail extends React.Component<any> {
	render() {
		let style = {margin: '10px'};
		style = this.props.equipment.left <= 0 ? Object.assign(style, {color: 'grey'}) : style;

		const input = this.props.equipment.left <= 0 && !this.props.equipment.taken ?
			<input type="checkbox" disabled/> :
			<input type="checkbox" checked={this.props.equipment.taken} onChange={this.handleCheckboxClick}/>;

		return (
			<div style={style}>
				{input} {this.props.equipment.name}
			</div>
		);
	}

	/**
	 * Click handler for the check box.
	 * Each checked values will do a different delegate to equipmentList.
	 * @param event
	 */
	handleCheckboxClick = (event) => {
		if (event.target.checked) {
			equipmentList.takeOne(this.props.equipment.id, this.props.userState.userId);
		} else {
			equipmentList.returnOne(this.props.equipment.id, this.props.userState.userId);
		}
	}
}

export default EquipmentDetail;
