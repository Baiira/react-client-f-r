import * as React from 'react';
import EquipmentDetail from './EquipmentDetail';
import {observer} from 'mobx-react';
import equipmentList from '../../../mobx/observables/EquipmentList';

@observer
class EquipmentListView extends React.Component<any> {

	componentWillMount() {
		equipmentList.getAllEquipments(this.props.userState.userId);
	}

	render() {
		const listStyle = {
			position: 'absolute' as "absolute",
			top: '200px',
			left: '200px'
		};

		const toPrint = [];

		equipmentList.equipments.forEach(val =>
			toPrint.push(
				<EquipmentDetail key={val.id} equipment={val} userState={this.props.userState}/>
			));
		return (
			<div style={listStyle}>
				Equipment List:
				{toPrint}
			</div>
		);
	}
}

export default EquipmentListView;
