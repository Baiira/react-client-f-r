import * as React from 'react';
import Webcam from '../webcam/ReactWebcam';
import EquipmentList from './equipments/EquipmentListView';
import {observer} from 'mobx-react';
import userState from '../../mobx/observables/UserState';

@observer
class ViewSelect extends React.Component<any> {
	userState;

	componentWillMount() {
		this.userState = userState;
	}

  render() {
		const btnStyle = {
			position: "absolute" as "absolute",
			left: '150px',
			top: '50px',
		};

		const webcamStyle = {
			height: '320px',
			width: '640px',
			top: '50px',
			right: '0',
			left: undefined
		};

		return (
      <div>
        <button onClick={this.props.appState.disconnect} style={btnStyle}>Identification</button>
        <Webcam style={webcamStyle}/>
				<div>
					<EquipmentList userState={this.userState}/>
				</div>
      </div>
    );
  }
}

export default ViewSelect;
